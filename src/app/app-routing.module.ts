import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { FormbuildComponent } from './formbuild/formbuild.component';
import { RegisterComponent } from './authentication/register/register.component';
import { LoginComponent } from './authentication/login/login.component';
import { PasswordChangeComponent } from './authentication/password-change/password-change.component';

const routes: Routes = [

  {   
     path:'',
     component:AppComponent,
  },


  {
    path:'signup',
    component:RegisterComponent

  },
  {
    path:'login',
    component:LoginComponent,
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
