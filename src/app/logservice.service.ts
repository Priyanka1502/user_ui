import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { constant } from 'src/app/constant';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LogserviceService {

  constructor(private http:HttpClient) { }

  register(obj:object):Observable <any>{
    return this.http.post(constant.API_URL+'sign/',obj,{})
    .pipe(map((res:any)=>res))
  }
  login(obj:object):Observable <any>{
    return this.http.post(constant.API_URL+'auth/login',obj,{})
    .pipe(map((res:any)=>res))
  }
}





