import { Component, OnInit } from '@angular/core';
import{ FormBuilder,FormGroup,Validator, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { LogserviceService } from 'src/app/logservice.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  userResult:any
  UserForm: FormGroup;
  constructor(private logservice:LogserviceService,private studentform:FormBuilder,private router:Router) { }
  
  ngOnInit() {
    this.UserForm=this.studentform.group({
      name:[''],
      phone:[''],
      email:['',[
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      
      ]],
      password:['',Validators.required],


    });
  }
    OnSubmit(){ 
    this.logservice.register(this.UserForm.value).subscribe((data)=>{
      console.log(data)
      if(data){
        this.userResult=data
        console.log(this.userResult)

        this.router.navigateByUrl('login')
      }
    })
   }


}
