import { Component, OnInit } from '@angular/core';
import{ FormBuilder,FormGroup,Validator, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { LogserviceService } from 'src/app/logservice.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
  loginresult: any;
  constructor(private logservice:LogserviceService,private studentform:FormBuilder,private router:Router) { }
  
  ngOnInit() {
    this.LoginForm=this.studentform.group({
     
      email:['',[
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      
      ]],
      password:['',Validators.required]


    });
  }
    OnSubmit(){ 
    this.logservice.login(this.LoginForm.value).subscribe((data)=>{
      if(data){
        console.log(data)
        this.loginresult=data
       
      }
    })
   }

}


